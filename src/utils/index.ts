import { timeFormat } from "d3-time-format";

export function average(array: number[]): number {
    const avg = array.reduce((a: number, b: number) => a + b) / array.length;
    return parseFloat(avg.toFixed(2));
}

export const formatDate = timeFormat("%b %d, '%y");
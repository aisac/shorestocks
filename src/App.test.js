import { render, screen } from '@testing-library/react';
import App from './App';

test('App mounts and renders title', () => {
  render(<App />);
  const linkElement = screen.getByText(/ShoreStocks/i);
  expect(linkElement).toBeInTheDocument();
});

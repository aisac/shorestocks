/** Type used to store main App state */
export type AppState = {
    /** Store a selected Stock */
    selectedStock: Stock | null,
    /** Store a list of Stocks marked as favorites - @todo */
    favorites?: Stock[]
}

/** Interface for actions used by Reducer */
export interface ReducerAction {
    type: ACTION_TYPE,
    payload: AppState
}

/** Action types Enum */
export enum ACTION_TYPE {
    /** Used for setting a selected stock into state */
    SET_STOCK = 'set-stock',
    /** Used for setting a list of favories into state - @todo */
    SET_FAVORITES = 'set-favorites'
}

/** Enum with actions for Toolbar onChange method */
export enum PERIOD_CHANGE {
    INTERVAL = 'interval',
    START_DATE = 'start-date',
    END_DATE = 'end-date'
}

/** Type used to store a Stock object */
export type Stock = {
    description: string,
    displaySymbol: string,
    symbol: string,
    type: string
}

/** Type used to store a Stock close price for a specific date */
export type StockPrice = {
    /** Date mark */
    date: number;
    /** Price for the coresponding date */
    close: number;
}

/** Contains all the details for a specific Stock */
export type StockDetails = {
    country: string,
    currency: string,
    exchange: string,
    finnhubIndustry: string,
    ipo: string,
    logo: string,
    marketCapitalization: number,
    name: string,
    phone: string,
    shareOutstanding: number
    ticker: string,
    weburl: string
}

/** Type used for the Stock Chart component */
export type AreaProps = {
    width: number;
    height: number;
    data: Array<StockPrice>;
    average: boolean;
    margin?: { top: number; right: number; bottom: number; left: number };
  };

/** Type used for the Toolbar component */
export type PeriodToolbarType = {
    value: string,
    startDate: number,
    endDate: number,
    onChange: (action: PERIOD_CHANGE, value: string | number) => void
}
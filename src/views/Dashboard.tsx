import React, { useContext } from 'react';
import { Grid } from '@material-ui/core';
import Search from '../components/Search';
import DashboardContent from '../components/DashboardContent';
import { AppContext } from '../contexts/AppContext';

export default function Dashboard() {

  const { state } = useContext(AppContext);
  return (
    <div style={{ padding: 20 }}>
      <Grid container justify="center" alignItems="center" spacing={3}>
        <Grid item xs={10}>
          <Search />
        </Grid>          
        {state.selectedStock && <DashboardContent />}
      </Grid>
    </div>
  )
}
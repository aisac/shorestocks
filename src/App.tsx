import React from 'react';
import {
  HashRouter,
  Switch,
  Route
} from 'react-router-dom';

import Navigation from './components/Navigation';
import Dashboard from './views/Dashboard';
import Favorites from './views/Favorites';

import { AppProvider } from './contexts/AppContext';



export default function App() {

  return (
    <HashRouter>
      <Navigation />

      <AppProvider>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/favorites" component={Favorites} />
        </Switch>
      </AppProvider>
    </HashRouter>
  )
}
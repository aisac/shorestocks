import React, { useContext, useEffect } from 'react';
import { Box, Switch, FormControlLabel, Grid } from '@material-ui/core';
import StockChart from '../components/StockChart';
import ParentSize from '@visx/responsive/lib/components/ParentSize';

import { AppContext } from '../contexts/AppContext';
import { PERIOD_CHANGE, StockPrice } from '../types';
import { getUnixTime, subBusinessDays } from 'date-fns';
import PeriodToolbar from './PeriodToolbar';

const endUnixTime: number = getUnixTime(subBusinessDays(new Date(), 1));
const startUnixTime: number = getUnixTime(subBusinessDays(new Date(), 2));

export default function DashboardContent() {
    
    const [data, setData] = React.useState<Array<StockPrice>>([]);
    const [average, setAverage] = React.useState<boolean>(false);
    const [interval, setInterval] = React.useState<string>('1');
    const [startDate, setStartDate] = React.useState<number>(startUnixTime);
    const [endDate, setEndDate] = React.useState<number>(endUnixTime);
    const { state } = useContext(AppContext);

    useEffect(() => {
        (async () => {
            const response = await fetch(`https://finnhub.io/api/v1/stock/candle?symbol=${state.selectedStock?.symbol}&resolution=${interval}&from=${startDate}&to=${endDate}&token=c07k05n48v6retjai5lg`);
            const _data: any = await response.json();
            
            setData(_data.c.map((c: number, index: number) => {
                return {
                    date: _data.t[index] * 1000,
                    close: c
                }
            }));
        })()
    }, [state.selectedStock, interval, startDate, endDate]);

    const onSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setAverage(event.target.checked);
    }

    const onPeriodChange = (action: PERIOD_CHANGE, value: string | number): void => {
        switch(action) {
            case PERIOD_CHANGE.INTERVAL:
                setInterval(value as string);
                break;
            case PERIOD_CHANGE.START_DATE:
                setStartDate(value as number);
                break;
            case PERIOD_CHANGE.END_DATE:
                setEndDate(value as number);
                break;
        }
        
    }

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <PeriodToolbar
                    value={interval}
                    startDate={startDate}
                    endDate={endDate}
                    onChange={onPeriodChange} />
            </Grid>
            <Grid item xs={12}>
                <Box height={300}>
                    <ParentSize>{({ width, height }) => 
                    <StockChart
                        width={width}
                        height={height}
                        data={data}
                        average={average}
                    />}
                </ParentSize>
                </Box>
                <FormControlLabel control={<Switch name="avgCheckbox" onChange={onSwitchChange} />} label="Toggle average value" />
            </Grid>            
        </Grid>
    )
}





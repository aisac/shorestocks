import React, { ChangeEvent } from 'react';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { PeriodToolbarType, PERIOD_CHANGE } from '../types';
import { Grid, TextField } from '@material-ui/core';
import { format, fromUnixTime, getUnixTime } from 'date-fns';

export default function PeriodToolbar(props: PeriodToolbarType) {

    const onToggleChange = (event: React.MouseEvent<HTMLElement, MouseEvent>, value: any) => {
        props.onChange(PERIOD_CHANGE.INTERVAL, value);
    };

    const onStartChange = (event: ChangeEvent<HTMLInputElement>) => {
        props.onChange(PERIOD_CHANGE.START_DATE, getUnixTime(new Date(event.target.value)));
    };

    const onEndChange = (event: ChangeEvent<HTMLInputElement>) => {
        props.onChange(PERIOD_CHANGE.END_DATE, getUnixTime(new Date(event.target.value)));
    };

    const startDate = format(fromUnixTime(props.startDate), "yyyy-MM-dd");
    const endDate = format(fromUnixTime(props.endDate), "yyyy-MM-dd")

    return (    
        <Grid container spacing={3}>
            <Grid item sm={6}>
            <ToggleButtonGroup
                size="small"
                value={props.value}
                exclusive
                onChange={onToggleChange}
                >
                <ToggleButton value="1">
                {'1m'}
                </ToggleButton>
                <ToggleButton value="5">
                    {'5m'}
                </ToggleButton>
                <ToggleButton value="30" aria-label="right aligned">
                    {'30'}
                </ToggleButton>
                <ToggleButton value="60" aria-label="justified">
                    {'60'}
                </ToggleButton>
                <ToggleButton value="D" aria-label="justified">
                    {'1D'}
                </ToggleButton>
                <ToggleButton value="W" aria-label="justified">
                    {'1W'}
                </ToggleButton>
                <ToggleButton value="M" aria-label="justified">
                    {'1M'}
                </ToggleButton>
            </ToggleButtonGroup>

            </Grid>
            <Grid item sm={3}>
            <TextField
                id="start-date"
                label="Start date"
                type="date"
                onChange={onStartChange}
                defaultValue={startDate}
                InputLabelProps={{
                shrink: true,
                }}
            />
            </Grid>
            <Grid item sm={3}>
            <TextField
                id="end-date"
                label="End date"
                type="date"
                onChange={onEndChange}
                defaultValue={endDate}
                InputLabelProps={{
                shrink: true,
                }}
            />
            </Grid>
        </Grid>
    )
}







 
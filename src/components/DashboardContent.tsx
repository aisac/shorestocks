import React from 'react';
import { Grid } from '@material-ui/core';
import Details from './Details';
import ChartPanel from './ChartPanel';

export default function DashboardContent() {

    return (
        <>
            <Grid item xs={12} sm={8}>
                <ChartPanel />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Details />
            </Grid>
        </>
    )
}





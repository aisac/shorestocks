import React, { ChangeEvent, useContext, useRef } from 'react';
import { AppContext } from '../contexts/AppContext';
import { Stock, ACTION_TYPE } from '../types';

import TextField from '@material-ui/core/TextField';
import Autocomplete, { AutocompleteChangeReason } from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import fetch from 'cross-fetch';
import { debounce } from 'lodash';
import { FilterOptionsState } from '@material-ui/lab/useAutocomplete';

const DEBOUNCE_TIMEOUT: number = 500;

export default function Search() {
  const { state, dispatch } = useContext(AppContext);
  const [open, setOpen] = React.useState<boolean>(false);
  const [options, setOptions] = React.useState<Stock[]>([]);
  const loading = open && options.length === 0;


  const openDropdown = () => { setOpen(true); }
  const closeDropdown = () => { setOpen(false); }

  const onInputChange = (event: React.ChangeEvent<{}>, value: string, reason: string) => {
    if(value !== '' && reason !== 'reset') {
        doSearch.current(value);
    }
  }

  const doSearch = useRef(
    debounce(async (value: string) => {
      const response = await fetch(`https://finnhub.io/api/v1/search?q=${value}&token=c07k05n48v6retjai5lg`);
      const stockList = await response.json();

      if(stockList.count > 0) {
          setOptions(stockList.result);
      }
    }, DEBOUNCE_TIMEOUT)
  );

  const onChange = (event: ChangeEvent<{}>, value: Stock | null, reason: AutocompleteChangeReason) => {
      if(value !== null) {
        dispatch({type: ACTION_TYPE.SET_STOCK, payload: { selectedStock: value }});
      }
  }

  const filterOptions = (options: Stock[], _state: FilterOptionsState<Stock>): Stock[] => options;
  const getOptionLabel = (option: Stock): string => `(${option.symbol}) ${option.description}`;
  
  return (
    <Autocomplete
      open={open}
      onOpen={openDropdown}
      onClose={closeDropdown}
      onInputChange={onInputChange}
      onChange={onChange}
      getOptionLabel={getOptionLabel}
      filterOptions={filterOptions}
      options={options}
      loading={loading}
      value={state.selectedStock}
      loadingText="Searching for sugestions..."
      renderInput={(params) => (
        <TextField
          {...params}
          label="Search for a stock name"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                <InputAdornment position="end">
                    <SearchIcon />
                </InputAdornment>
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}

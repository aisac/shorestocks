import { createStyles, Grid, makeStyles, Theme, Typography } from '@material-ui/core';
import React, { useContext, useEffect } from 'react';
import { AppContext } from '../contexts/AppContext';
import { StockDetails } from '../types';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        img: {
            margin: 'auto',
            display: 'block',
            maxWidth: '100%',
            maxHeight: '100%',
        }
    })
);

export default function Details() {
    
    const [data, setData] = React.useState<StockDetails | null>(null);
    const { state } = useContext(AppContext);

    useEffect(() => {
        (async () => {            
            const response = await fetch(`https://finnhub.io/api/v1/stock/profile2?symbol=${state.selectedStock?.symbol}&token=c07k05n48v6retjai5lg`);
            const _data: StockDetails = await response.json();
            setData(_data);
        })()
    }, [state.selectedStock]);

    const classes = useStyles();

    return (
        <Grid container spacing={3} alignItems="center">
            <Grid item xs={8}>
                <Typography variant="h4" component="h4">{data?.name}</Typography>
            </Grid>
            <Grid item xs={4}>
                <img className={classes.img} alt="complex" src={data?.logo} />
            </Grid>

            <Grid item xs={4}>
                <Typography variant="h6" component="h6">Ticker</Typography>
            </Grid>
            <Grid item xs={8}>
                <Typography variant="body1" component="span">{data?.ticker}</Typography>
            </Grid>

            <Grid item xs={4}>
                <Typography variant="h6" component="h6">Exchange</Typography>
            </Grid>
            <Grid item xs={8}>
                <Typography variant="body1" component="span">{data?.exchange}</Typography>
            </Grid>
            
            <Grid item xs={4}>
                <Typography variant="h6" component="h6">Market capitalization</Typography>
            </Grid>
            <Grid item xs={8}>
                <Typography variant="body1" component="span">{data?.marketCapitalization}</Typography>
            </Grid>
            
            <Grid item xs={4}>
                <Typography variant="h6" component="h6">Industry</Typography>
            </Grid>
            <Grid item xs={8}>
                <Typography variant="body1" component="span">{data?.finnhubIndustry}</Typography>
            </Grid>
            
            <Grid item xs={4}>
                <Typography variant="h6" component="h6">IPO</Typography>
            </Grid>
            <Grid item xs={8}>
                <Typography variant="body1" component="span">{data?.ipo}</Typography>
            </Grid>
        </Grid>
    )
}






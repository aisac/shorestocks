
import { AppState, ReducerAction, ACTION_TYPE } from '../types';

export default function AppReducer(state: AppState, { type, payload }: ReducerAction): AppState {
    switch(type) {
        case ACTION_TYPE.SET_STOCK:
            const { selectedStock } = payload;
            return { ...state, selectedStock };
        case ACTION_TYPE.SET_FAVORITES:
            const { favorites } = payload;
            return { ...state, favorites };
        default: 
            throw new Error();
    }
}
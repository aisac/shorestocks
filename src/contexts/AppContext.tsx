import React, { createContext, useReducer, useContext } from "react";
import AppReducer from "../state/reducer";
import { AppState } from "../types";


interface AppContextType {
  state: AppState,
  dispatch: any
}

const initialState: AppState = {
  selectedStock: null
};

export const AppContext = createContext<AppContextType>({state: initialState, dispatch: () => null});

function AppProvider(props: any){
  const [state, dispatch] = useReducer(AppReducer, initialState);

  const appData = { state, dispatch };

  return <AppContext.Provider value={appData} {...props} />;
}

function useAppContext() {
  return useContext(AppContext);
}

export { AppProvider, useAppContext };